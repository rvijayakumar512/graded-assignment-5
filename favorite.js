const movietype = new URLSearchParams(window.location.search).get('movietype');
const moviename = new URLSearchParams(window.location.search).get('moviename');
const id = new URLSearchParams(window.location.search).get('id');
const movieyear = new URLSearchParams(window.location.search).get('movieyear');
const container = document.querySelector('.favorite');

const renderFavorite = async () => {
  var doc;
  var boolflag=0;
  const oldres = await fetch('http://localhost:3000/favourit');
  const oldpost = await oldres.json();
  oldpost.forEach((oldpost) => {
    if(oldpost.title==moviename && oldpost.year==movieyear)
    {
      console.log(oldpost.title);
      console.log(moviename);
      boolflag=1;
      alert("Already added to Favorite")
    }
  });
  if(boolflag==0){
  if(movietype=="movies-coming"|| movietype=="movies-in-theaters"){
  const res = await fetch('http://localhost:3000/'+movietype+'/'+id);
  const post = await res.json();
  doc={
    title: post.title,
    year: post.year,
    genres: post.genres,
    ratings: post.ratings,
    poster: post.poster,
    contentRating: post.contentRating,
    duration: post.duration,
    releaseDate: post.releaseDate,
    averageRating: post.averageRating,
    originalTitle: post.originalTitle,
    storyline: post.storyline,
    actors: post.actors,
    imdbRating: post.imdbRating,
    posterurl: post.posterurl

  }
}
  else {
    const res = await fetch('http://localhost:3000/'+movietype);
    const post = await res.json();
    post.forEach((post) => {
      if(moviename==post.title && movieyear==post.year)
      {
        doc={
          title: post.title,
          year: post.year,
          genres: post.genres,
          ratings: post.ratings,
          poster: post.poster,
          contentRating: post.contentRating,
          duration: post.duration,
          releaseDate: post.releaseDate,
          averageRating: post.averageRating,
          originalTitle: post.originalTitle,
          storyline: post.storyline,
          actors: post.actors,
          imdbRating: post.imdbRating,
          posterurl: post.posterurl

      }

      }

    });
  }

  const newres = await fetch('http://localhost:3000/favourit', {method: 'POST',
    body: JSON.stringify(doc),
    headers: { 'Content-Type': 'application/json' }
  });
}
  window.location.replace('/index.html')
  }


window.addEventListener('DOMContentLoaded', renderFavorite);
