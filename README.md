# Graded Assignment 5

* This assignment is done using HTML,CSS,Javascript and some Bootstrap code.

* First clone this repository to your local system and then set up json-server
# File Description

* index.html and index.js => Home page logic
* details.html and details.js => View details of a movie logic
* favorite.html and favorite.js =>Add to favorite logic
* delete.html and delete.js => Delete from favorite logic


# Setting Up JSON SERVER
* Move to the cloned repository folder in command prompt.

* Run **npm run json:server** command in command prompt to start the json server. It reads data from data.json file.

* This will run on port 3000(http://localhost:3000/)
# Setting up Live Server
* Move to the cloned repository folder in command prompt.

* Run **live-server** command in another command prompt.

* This will run on port 8080 (http://127.0.0.1:8080/)

* Now test the functionalities implemented using the instructions mentioned below.



# Home Page and Search Movies
* On the live server you will find buttons displayed there for you to search the respective movie type.

* Once you click on a specific button the movies grouped together in that particular type will be displayed in the same page at the bottom in the form of tables.

* You should scroll down to see all the movies in that type.

* You can switch between the movietype you want by clicking on the appropriate button displayed in home page.


# View Details of a movie
* On the displayed table of movies you will find the movie title, movie poster, view movie details link and an add to favorite button.

* You can click on the view details link to see all the details of a particular movie that you selected.


# Add to Favorite list
* On the displayed table itself, you will find a add to favorite button.

* Click on that button to add that movie to your favorite list.

* If we click the same movie more than once to add to favorite list, an alert will be displayed that the movie is already added in favorite list.


# View Favorites
* In the home page there will be button to view favorites.

* You can click on that button to see all the movies added as favorite by the user.

* Also from the displayed list of favorite movies you can also see the details of that movie by clicking view details link.


# Remove from Favorites
* Click on view favorite button in home page, You will see all the movies added in favorite list.

* Now on the right side column of each favoite movie added you will find a button to remove that particular movie from favorite list.

* Click on that button to remove the movie from favorite list.

* To verify that the movie has been removed from favorite list, you can again click on view favorite button.

* Now you can see that the movie previouly removed will not be displayed as it is removed from favorite list.

* Also if all movies are deleted from favorite list and if user clicks on view favorites button, a message will be shown to user that no movies are addded currrently to favorite list
