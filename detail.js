const movietype = new URLSearchParams(window.location.search).get('movietype');
const moviename = new URLSearchParams(window.location.search).get('moviename');
const id = new URLSearchParams(window.location.search).get('id');
const movieyear = new URLSearchParams(window.location.search).get('movieyear');
const container = document.querySelector('.details');

const renderDetails = async () => {
  var template=``;
  if(movietype=="movies-coming"|| movietype=="movies-in-theaters"){
  const res = await fetch('http://localhost:3000/'+movietype+'/'+id);
  const post = await res.json();
    template += `
     <h1>${post.title}</h1>
     <img src ="img/${post.poster}" alt="Movie poster">
     <table>
     <tr>
     <td style="border: 1px solid;text-align:center">Id</td>
     <td style="border: 1px solid;text-align:left">${post.id}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Title</td>
     <td style="border: 1px solid;text-align:left">${post.title}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Year</td>
     <td style="border: 1px solid;text-align:left">${post.year}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Genres</td>
     <td style="border: 1px solid;text-align:left">${post.genres}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Ratings</td>
     <td style="border: 1px solid;text-align:left">${post.ratings}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Content Rating</td>
     <td style="border: 1px solid;text-align:left">${post.contentRating}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Duration</td>
     <td style="border: 1px solid;text-align:left">${post.duration}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Release Date</td>
     <td style="border: 1px solid;text-align:left">${post.releaseDate}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Average Rating</td>
     <td style="border: 1px solid;text-align:left">${post.averageRating}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Original Title</td>
     <td style="border: 1px solid;text-align:left">${post.originalTitle}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Story Line</td>
     <td style="border: 1px solid;text-align:left">${post.storyline}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Actors</td>
     <td style="border: 1px solid;text-align:left">${post.actors}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">IMDB Rating</td>
     <td style="border: 1px solid;text-align:left">${post.imdbRating}</td>
     </tr>
     <tr>
     <td style="border: 1px solid;text-align:center">Poster URL</td>
     <td style="border: 1px solid;text-align:left"><a href =${post.posterurl} >Poster Link</a></td>
     </tr>
     </table>
   `
   }
  else
  {
    const res = await fetch('http://localhost:3000/'+movietype);
    const post = await res.json();
    post.forEach(
      function(movie){
        if(movie.title==moviename && movieyear==movie.year)
        {
          console.log(movie.title);
          console.log(moviename);
        template += `
        <h1>${movie.title}</h1>
        <img src ="img/${movie.poster}" alt="Movie poster">
        <table>
        <tr>
        <td style="border: 1px solid;text-align:center">Title</td>
        <td style="border: 1px solid;text-align:left">${movie.title}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Year</td>
        <td style="border: 1px solid;text-align:left">${movie.year}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Genres</td>
        <td style="border: 1px solid;text-align:left">${movie.genres}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Ratings</td>
        <td style="border: 1px solid;text-align:left">${movie.ratings}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Content Rating</td>
        <td style="border: 1px solid;text-align:left">${movie.contentRating}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Duration</td>
        <td style="border: 1px solid;text-align:left">${movie.duration}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Release Date</td>
        <td style="border: 1px solid;text-align:left">${movie.releaseDate}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Average Rating</td>
        <td style="border: 1px solid;text-align:left">${movie.averageRating}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Original Title</td>
        <td style="border: 1px solid;text-align:left">${movie.originalTitle}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Story Line</td>
        <td style="border: 1px solid;text-align:left">${movie.storyline}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Actors</td>
        <td style="border: 1px solid;text-align:left">${movie.actors}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">IMDB Rating</td>
        <td style="border: 1px solid;text-align:left">${movie.imdbRating}</td>
        </tr>
        <tr>
        <td style="border: 1px solid;text-align:center">Poster URL</td>
        <td style="border: 1px solid;text-align:left"><a href =${movie.posterurl} >Poster Link</a></td>
        </tr>
        </table>
      `
      }

    })


  }
  container.innerHTML = template;
}


window.addEventListener('DOMContentLoaded', renderDetails);
